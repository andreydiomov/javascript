var TableActions = function() {
	var self = this;

	self.arrayOptions = [''];
	self.tableElement = null;
	self.nameInputElement = null;
	self.dateInputElement = null;
	self.cellCount = 5;

	self.init = function() {
		
		self.tableElement = document.getElementById('activities-table');
		self.nameInputElement = document.getElementById("name");
		self.dateInputElement = document.getElementById("date");

		self.addNewRowEventListener();

		sendRequest('GET', 'http://localhost:8080/app/itemHistory')
			.then(data => {
				data.items.forEach(option => {
					self.arrayOptions.push(option.name);
				});

				data.itemHistories.forEach(option => {
					self.addRow(option.item.id, option.created);					
				});
				self.arrayOptions.forEach((option, index) => {
					var optionElement = document.createElement("option");
					optionElement.textContent = option;
					optionElement.value = index;
					self.nameInputElement.appendChild(optionElement);
				});
			})
			.catch(err => console.log(err));
	}	

	self.addNewRowEventListener = function() {
		var newRowButtonElement = document.getElementById("add-row");
		newRowButtonElement.addEventListener("click", function() {
			if (self.nameInputElement.value == '') {
				alert("Выберите значение!");
				return;
			}
			self.addRow(self.nameInputElement.value, self.dateInputElement.value);
		})
	}

	self.addRow = function(itemId, created) {
		var itemName = itemId ?
				self.arrayOptions[itemId] :
			self.arrayOptions[self.nameInputElement.value];

		var createdDate = created ? self.stringToDate(created) : self.stringToDate(new Date());

		var rowCount = self.tableElement.rows.length;
		var row = self.tableElement.insertRow(rowCount);
		row.className = "read-mode";

		var nameTextElement = document.createElement("text");
		nameTextElement.innerHTML = itemName;
		nameTextElement.className = "read-control";

		var dateTextElement = document.createElement("text");
		dateTextElement.innerHTML = createdDate;
		dateTextElement.className = "read-control";

		var dateInputElement = document.createElement("input");
		dateInputElement.type = "datetime-local";
		dateInputElement.className = "edit-control";

		var isoDate = created ? new Date(created).toISOString() : new Date().toISOString();

		dateInputElement.value = isoDate.substring(0, isoDate.length - 1);

		var nameSelectElement = self.createSelectElement(itemId ? itemId : self.nameInputElement.value);
		nameSelectElement.className = "edit-control";

		var deleteButtonElement =
			self.createButtonElement("read-control", "Delete", function() {
				self.deleteRow(row.rowIndex)
			});
		var saveButtonElement =
			self.createButtonElement("edit-control", "Save", function() {
				self.saveRow(row, nameSelectElement, nameTextElement, dateInputElement, dateTextElement);
			});
		var cancelButtonElement =
			self.createButtonElement("edit-control", "Cancel", function() {
				self.setEditMode(row, false);
			});
		var updateButtonElement =
			self.createButtonElement("read-control", "Update", function() {
				self.updateRow(row, nameSelectElement, nameTextElement, dateInputElement, dateTextElement);
			});

		fillTableCells();
		self.clearAddInputs();

		function fillTableCells() {
			for (let index = 0; index < self.cellCount; index++) {
				var cell = row.insertCell(index)
				switch (index) {
					case 0:
						cell.innerHTML = rowCount;
						break;
					case 1:
						cell.appendChild(nameSelectElement);
						cell.appendChild(nameTextElement);
						break;
					case 2:
						cell.appendChild(dateInputElement);
						cell.appendChild(dateTextElement);
						break;
					case 3:
						cell.appendChild(deleteButtonElement);
						cell.appendChild(saveButtonElement);
						break;
					case 4:
						cell.appendChild(updateButtonElement);
						cell.appendChild(cancelButtonElement);
				}
			}
		}
	}

	self.stringToDate = function(stringDate)  {
		return moment(stringDate).format('DD/MM/YYYY hh:mm');
	}

	self.deleteRow = function(index) {
		var confirmResult = confirm('Are you sure?');
		if (confirmResult) {
			self.tableElement.deleteRow(index);
		}
	}

	self.saveRow = function(row, nameSelect, nameText, dateInput, dateText) {
		self.setEditMode(row, false);
		self.setTextValueFromInput(nameText, nameSelect);		
		dateText.innerHTML = self.stringToDate(dateInput.value);
	}

	self.updateRow = function(row, nameSelectElement, nameTextElement, dateInputElement, dateTextElement) {
		var editModeElements = document.getElementsByClassName('edit-mode');
		if (editModeElements.length > 0) {
			if (!confirm('You will lose your changes, ok?')) {
				return;
			}
		}

		self.disableEditModeForAll();
		self.setEditMode(row, true);

		nameSelectElement.value = nameTextElement.innerHTML;		
	}


	self.setTextValueFromInput = function(text, input) {
		var value = input.value;
		text.innerHTML = value;
	}

	self.setEditMode = function(row, enable) {
		row.className = enable ? "edit-mode" : "read-mode";
	}

	self.disableEditModeForAll = function() {
		var editModeElements = document.getElementsByClassName('edit-mode');
		for (element of editModeElements) {
			element.className = 'read-mode';
		}
	}

	self.clearAddInputs = function() {
		self.nameInputElement.value = "";
		self.dateInputElement.value = "";
	}

	self.createButtonElement = function(className, innerHTML, clickFunction) {
		var element = document.createElement("button");

		if (className)
			element.className = className;

		if (innerHTML)
			element.innerHTML = innerHTML;

		if (clickFunction)
			element.addEventListener('click', function(e) { clickFunction() });

		return element;
	}

	self.createSelectElement = function(selectedValue) {

		var selectElement = document.createElement("select");
		selectElement.className = "activities-select";

		self.arrayOptions.forEach(option => {
			var optionElement = document.createElement("option");
			optionElement.textContent = option;
			optionElement.value = option;

			selectElement.appendChild(optionElement);
		});

		if (selectedValue) {
			selectElement.value = selectedValue;
		}

		return selectElement;
	}
}